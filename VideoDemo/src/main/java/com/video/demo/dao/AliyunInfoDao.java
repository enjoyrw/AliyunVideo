package com.video.demo.dao;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

public interface AliyunInfoDao {
	/**
	 * 根据阿里云转码成功返回的videoId,duration,fileUrl更新视频信息
	 * @param condition
	 */
	public void updateVideoInfo(@Param("time")Date time,@Param("fileUrl") String fileUrl,@Param("aliyunVideoId") String aliyunVideoId);
}
