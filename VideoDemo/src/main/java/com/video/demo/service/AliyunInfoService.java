package com.video.demo.service;

import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.video.demo.dto.AliyunImageInfoDto;
import com.video.demo.dto.AliyunVideoInfoDto;

public interface AliyunInfoService {
	/**
	 * 获取视频播放凭证
	 * @return
	 */
	public GetVideoPlayAuthResponse getVideoPlayAuth(String videoId);
	/**
	 * 获取视频上传地址和凭证
	 * @return
	 */
	public AliyunVideoInfoDto getVideoUploadInfo(String fileName,String title,String coverURL,Integer cateId,String tags,String description,Long fileSize);
	/**
	 * 刷新视频上传凭证
	 * @return
	 */
	public String refreshVideoUploadAtuh(String videoId);
	/**
	 * 获取图片上传地址和凭证
	 * @return
	 */
	public AliyunImageInfoDto getImageUploadInfo(String imageType,String imageExt);
	
	/**
	 * 根据aliyunVideoId更新视频
	 * @param status
	 * @param aliyunVideoId
	 * @param duration
	 * @param fileUrl
	 */
	public void transcodeComplete(String status,String aliyunVideoId,String duration,String fileUrl);
	
}
