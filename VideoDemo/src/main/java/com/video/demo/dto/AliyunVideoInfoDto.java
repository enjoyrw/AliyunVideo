package com.video.demo.dto;

/**
 * 阿里云视频dto
 * @author renwei
 *
 */
public class AliyunVideoInfoDto {
	// 请求ID
	private String requestId;
	// 视频ID
	private String videoId;
	// 上传地址
	private String uploadAddress;
	// 上传凭证
	private String uploadAuth;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}

	public String getUploadAddress() {
		return uploadAddress;
	}

	public void setUploadAddress(String uploadAddress) {
		this.uploadAddress = uploadAddress;
	}

	public String getUploadAuth() {
		return uploadAuth;
	}

	public void setUploadAuth(String uploadAuth) {
		this.uploadAuth = uploadAuth;
	}
}
