package com.video.demo.dto;

public class AliyunImageInfoDto {
	// 请求ID
	private String requestId;
	// 上传地址
	private String uploadAddress;
	// 上传凭证
	private String uploadAuth;
	// 图片地址
	private String imageUrl;
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getUploadAddress() {
		return uploadAddress;
	}
	public void setUploadAddress(String uploadAddress) {
		this.uploadAddress = uploadAddress;
	}
	public String getUploadAuth() {
		return uploadAuth;
	}
	public void setUploadAuth(String uploadAuth) {
		this.uploadAuth = uploadAuth;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
}
