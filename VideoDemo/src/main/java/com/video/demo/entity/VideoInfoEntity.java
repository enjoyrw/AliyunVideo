package com.video.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.video.demo.utils.DateUtil;

import java.io.Serializable;
import java.util.Date;


/**
 * 微视频表
 *
 * @author CHENQUAN
 * @date 2017-06-21 10:23:09
 */
public class VideoInfoEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//视频唯一ID
    private Long videoId;
    //创建人ID
    private Long createrId;
    //视频名称
    private String videoName;
    //视频URL地址
    private String videoUrl;
    //视频时长
    private Date duration;
    //封面图片URL
    private String imgUrl;
    //学科代码ES2016-00002
    private Integer disciplineCode;
    //适用年级。如一年级1；二年级2……高中一年级10；高中二年级11；高中三年级12
    private Integer gradeNo;
    //被分享次数
    private Integer favorites;
    //点赞数
    private Integer likes;
    //被踩数
    private Integer unlikes;
    //是否删除
    private Boolean isDeleted;
    //删除时间
    private Date delTime;
    //观看次数
    private Integer watchs;
    //是否公开
    private Boolean isOpened;
    //是否创建完成
    private Boolean isCreationComplete;
    //创建时间
    private Date createTime;
    //修改时间
    private Date updateTime;
    //章节唯一ID
    private Integer categoryId;
    //章节名称
    private String categoryName;
    //阿里云videoId
    private String aliyunVideoId;
    

	public String getAliyunVideoId() {
		return aliyunVideoId;
	}

	public void setAliyunVideoId(String aliyunVideoId) {
		this.aliyunVideoId = aliyunVideoId;
	}

	/**
     * constructors
     */
    public VideoInfoEntity() {
    }

    public VideoInfoEntity(Long videoId) {
        this.videoId = videoId;
    }

    public VideoInfoEntity(Long videoId, Boolean isCreationComplete) {
        this(videoId);
        this.isCreationComplete = isCreationComplete;
    }

    public VideoInfoEntity(Long videoId, Boolean isDeleted, Date delTime) {
        this.videoId = videoId;
        this.isDeleted = isDeleted;
        this.delTime = delTime;
    }

    public VideoInfoEntity(Long videoId, Long createrId, String videoName, String videoUrl, String imgUrl, Integer disciplineCode, Integer gradeNo, Boolean isOpened) {
        this.videoId = videoId;
        this.createrId = createrId;
        this.videoName = videoName;
        this.videoUrl = videoUrl;
        this.imgUrl = imgUrl;
        this.disciplineCode = disciplineCode;
        this.gradeNo = gradeNo;
        this.isOpened = isOpened;
    }

    public VideoInfoEntity(Long videoId, Long createrId, String videoName, String videoUrl, String imgUrl, Integer categoryId, String categoryName, Integer disciplineCode, Integer gradeNo, Boolean isOpened) {
        this.videoId = videoId;
        this.createrId = createrId;
        this.videoName = videoName;
        this.videoUrl = videoUrl;
        this.imgUrl = imgUrl;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.disciplineCode = disciplineCode;
        this.gradeNo = gradeNo;
        this.isOpened = isOpened;
    }
    
    public VideoInfoEntity(Long videoId, Long createrId, String videoName, String videoUrl, String imgUrl, Integer categoryId, String categoryName, Integer disciplineCode, Integer gradeNo, Boolean isOpened,String aliyunVideoId) {
        this.videoId = videoId;
        this.createrId = createrId;
        this.videoName = videoName;
        this.videoUrl = videoUrl;
        this.imgUrl = imgUrl;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.disciplineCode = disciplineCode;
        this.gradeNo = gradeNo;
        this.isOpened = isOpened;
        this.aliyunVideoId = aliyunVideoId;
    }
    public VideoInfoEntity(Long videoId,  String videoUrl, Date duration,String aliyunVideoId) {
        this.videoId = videoId;
        this.videoUrl = videoUrl;
        this.duration = duration;
        this.aliyunVideoId = aliyunVideoId;
    }
    public VideoInfoEntity(Long createrId, String videoName, String imgUrl, Integer categoryId, String categoryName, Integer disciplineCode, Integer gradeNo, Boolean isOpened,String aliyunVideoId) {
        this.createrId = createrId;
        this.videoName = videoName;
        this.imgUrl = imgUrl;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.disciplineCode = disciplineCode;
        this.gradeNo = gradeNo;
        this.isOpened = isOpened;
        this.aliyunVideoId = aliyunVideoId;
    }
    

    public VideoInfoEntity(Long videoId, Integer favorites, Integer likes, Integer unlikes, Integer watchs) {
        this.videoId = videoId;
        this.favorites = favorites;
        this.likes = likes;
        this.unlikes = unlikes;
        this.watchs = watchs;
    }

    /**
     * 设置：视频唯一ID
     *
     * @param videoId 视频唯一ID
     */
    public void setVideoId(Long videoId) {
        this.videoId = videoId;
    }

    /**
     * 获取：视频唯一ID
     *
     * @return videoId 视频唯一ID
     */
    public Long getVideoId() {
        return videoId;
    }

    /**
     * 设置：创建人ID
     *
     * @param createrId 创建人ID
     */
    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    /**
     * 获取：创建人ID
     *
     * @return createrId 创建人ID
     */
    public Long getCreaterId() {
        return createrId;
    }

    /**
     * 设置：视频名称
     *
     * @param videoName 视频名称
     */
    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    /**
     * 获取：视频名称
     *
     * @return videoName 视频名称
     */
    public String getVideoName() {
        return videoName;
    }

    /**
     * 设置：视频URL地址
     *
     * @param videoUrl 视频URL地址
     */
    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    /**
     * 获取：视频URL地址
     *
     * @return videoUrl 视频URL地址
     */
    public String getVideoUrl() {
        return videoUrl;
    }

    /**
     * 设置：视频时长
     *
     * @param duration 视频时长
     */
    public void setDuration(Date duration) {
        this.duration = duration;
    }

    /**
     * 获取：视频时长
     *
     * @return duration 视频时长
     */
    @JsonFormat(pattern = "HH:mm:ss", timezone = "GMT+8")
    public Date getDuration() {
        return duration;
    }

    /**
     * 设置：封面图片URL
     *
     * @param imgUrl 封面图片URL
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    /**
     * 获取：封面图片URL
     *
     * @return imgUrl 封面图片URL
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     * 设置：学科代码ES2016-00002
     *
     * @param disciplineCode 学科代码ES2016-00002
     */
    public void setDisciplineCode(Integer disciplineCode) {
        this.disciplineCode = disciplineCode;
    }

    /**
     * 获取：学科代码ES2016-00002
     *
     * @return disciplineCode 学科代码ES2016-00002
     */
    public Integer getDisciplineCode() {
        return disciplineCode;
    }

    /**
     * 设置：适用年级。如一年级1；二年级2……高中一年级10；高中二年级11；高中三年级12
     *
     * @param gradeNo 适用年级。如一年级1；二年级2……高中一年级10；高中二年级11；高中三年级12
     */
    public void setGradeNo(Integer gradeNo) {
        this.gradeNo = gradeNo;
    }

    /**
     * 获取：适用年级。如一年级1；二年级2……高中一年级10；高中二年级11；高中三年级12
     *
     * @return gradeNo 适用年级。如一年级1；二年级2……高中一年级10；高中二年级11；高中三年级12
     */
    public Integer getGradeNo() {
        return gradeNo;
    }

    /**
     * 设置：被分享次数
     *
     * @param favorites 被分享次数
     */
    public void setFavorites(Integer favorites) {
        this.favorites = favorites;
    }

    /**
     * 获取：被分享次数
     *
     * @return favorites 被分享次数
     */
    public Integer getFavorites() {
        return favorites;
    }

    /**
     * 设置：点赞数
     *
     * @param likes 点赞数
     */
    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    /**
     * 获取：点赞数
     *
     * @return likes 点赞数
     */
    public Integer getLikes() {
        return likes;
    }

    /**
     * 设置：被踩数
     *
     * @param unlikes 被踩数
     */
    public void setUnlikes(Integer unlikes) {
        this.unlikes = unlikes;
    }

    /**
     * 获取：被踩数
     *
     * @return unlikes 被踩数
     */
    public Integer getUnlikes() {
        return unlikes;
    }

    /**
     * 设置：是否删除
     *
     * @param isDeleted 是否删除
     */
    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 获取：是否删除
     *
     * @return isDeleted 是否删除
     */
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置：删除时间
     *
     * @param delTime 删除时间
     */
    public void setDelTime(Date delTime) {
        this.delTime = delTime;
    }

    /**
     * 获取：删除时间
     *
     * @return delTime 删除时间
     */
    public Date getDelTime() {
        return delTime;
    }

    /**
     * 设置：观看次数
     *
     * @param watchs 观看次数
     */
    public void setWatchs(Integer watchs) {
        this.watchs = watchs;
    }

    /**
     * 获取：观看次数
     *
     * @return watchs 观看次数
     */
    public Integer getWatchs() {
        return watchs;
    }

    /**
     * 设置：是否公开
     *
     * @param isOpened 是否公开
     */
    public void setIsOpened(Boolean isOpened) {
        this.isOpened = isOpened;
    }

    /**
     * 获取：是否公开
     *
     * @return isOpened 是否公开
     */
    public Boolean getIsOpened() {
        return isOpened;
    }

    /**
     * 设置：是否创建完成
     *
     * @param isCreationComplete 是否创建完成
     */
    public void setIsCreationComplete(Boolean isCreationComplete) {
        this.isCreationComplete = isCreationComplete;
    }

    /**
     * 获取：是否创建完成
     *
     * @return isCreationComplete 是否创建完成
     */
    public Boolean getIsCreationComplete() {
        return isCreationComplete;
    }

    /**
     * 设置：创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取：创建时间
     *
     * @return createTime 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置：修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取：修改时间
     *
     * @return updateTime 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "VideoInfoEntity{" +
                "videoId=" + videoId +
                ", createrId=" + createrId +
                ", videoName='" + videoName + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", duration=" + duration +
                ", imgUrl='" + imgUrl + '\'' +
                ", disciplineCode=" + disciplineCode +
                ", gradeNo=" + gradeNo +
                ", favorites=" + favorites +
                ", likes=" + likes +
                ", unlikes=" + unlikes +
                ", isDeleted=" + isDeleted +
                ", delTime=" + delTime +
                ", watchs=" + watchs +
                ", isOpened=" + isOpened +
                ", isCreationComplete=" + isCreationComplete +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }

    public String insertSql() {
        return "INSERT INTO `video_info` (video_id,creater_id,video_name,video_url,duration,img_url,discipline_code,grade_no,is_opened,create_time,category_id,category_name) " +
                "SELECT " + videoId + "," + createrId + ",'" + videoName + "','" + videoUrl + "','" + DateUtil.dateToString(duration, "HH:mm:ss") + "','" + imgUrl + "'," + disciplineCode
                + "," + gradeNo + "," + isOpened + ",'" + DateUtil.dateToString(createTime) + "'," + categoryId + ",'" + categoryName + "';\n";
    }
}
