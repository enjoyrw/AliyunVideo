package com.video.demo.entity;


import java.util.Date;

/**
 * 统一返回结果类
 * Created by CHQIU on 2017-06-20.
 */
public class BaseResult {

    // 状态码：1成功，其他均为失败【详见错误状态码表】
    public int code;

    // 成功为success，其他为失败原因
    public String message;

    // 数据结果集
    public Object data;
    //当前时间
    public long time;

    public BaseResult(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.time = new Date().getTime();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
