package com.video.demo.entity;

import com.video.demo.utils.ResultConstant;



/**
 * Created by CHQIU on 2017-06-20.
 */
public class Result extends BaseResult {

    public Result(ResultConstant resultConstant, Object data) {
        super(resultConstant.code, resultConstant.message, data);
    }

    /**
     * 只返回状态码不返回内容
     *
     * @param resultConstant
     */
    public Result(ResultConstant resultConstant) {
        super(resultConstant.code, resultConstant.message, null);
    }

    public Result() {
        super(0, null, null);
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
