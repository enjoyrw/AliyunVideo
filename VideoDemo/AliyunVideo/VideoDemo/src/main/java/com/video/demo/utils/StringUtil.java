package com.video.demo.utils;


import com.video.demo.enums.FormatEnums;

public class StringUtil {
	/**
	 * 字符串是否为空
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str){
		if(str==null||"".equals(str)||"null".equals(str)||str.trim().length()==0){
			return true;
		}
		return false;
	}
	public static boolean isNotBlank(String str){
		return !isBlank(str);
	}
	/**
	 * 格式是否正确
	 * @param videoName
	 * @return
	 */
	public static boolean validateVideoFormat(String videoName){
		if(isBlank(videoName)){
			return false;
		}
		if(videoName.indexOf(".")>0){
			String suffix= videoName.substring(videoName.lastIndexOf("."),videoName.length());
			//格式是否符合要求
			if(FormatEnums.getFormat(suffix)!=null)
				return true;
		}
		return false;
	}
}
