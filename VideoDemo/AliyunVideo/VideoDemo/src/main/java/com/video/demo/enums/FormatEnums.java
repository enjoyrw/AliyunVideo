package com.video.demo.enums;
/**
 * 格式枚举
 * @author renwei
 *
 */
public enum FormatEnums {
	GP3(".3gp"), 
	ASF(".asf"), 
	AVI(".avi"), 
	DAT(".dat"), 
	DV(".dv"), 
	FLV(".flv"), 
	F4V(".f4v"), 
	GIF(".gif"), 
	M2T(".m2t"), 
	M4V(".m4v"), 
	MJ2(".mj2"), 
	MJPEG(".mjpeg"), 
	MKV(".mkv"), 
	MOV(".mov"), 
	MP4(".mp4"), 
	MPE(".mpe"), 
	MPG(".mpg"), 
	MPEG(".mpeg"), 
	MTS(".mts"), 
	OGG(".ogg"), 
	QT(".qt"), 
	RM(".rm"), 
	RMVB(".rmvb"), 
	SWF(".swf"), 
	TS(".ts"), 
	VOB(".vob"), 
	WMV(".wmv"), 
	WEBM(".webm"), 
	AAC(".aac"), 
	AC3(".ac3"), 
	ACM(".acm"), 
	AMR(".amr"), 
	APE(".ape"), 
	CAF(".caf"), 
	FLAC(".flac"), 
	M4A(".m4a"), 
	MP3(".mp3"), 
	RA(".ra"), 
	WAV(".wav"), 
	WMA(".wma");
	private String format;

	private FormatEnums(String format) {
		this.format = format;
	}

	public String getFormat() {
		return format;
	}

	public static FormatEnums getFormat(String format) {
		for (FormatEnums fe : FormatEnums.values()) {
			if (fe.getFormat().equals(format)) {
				return fe;
			}
		}
		return null;
	}
}
