package com.video.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.video.demo.dto.AliyunImageInfoDto;
import com.video.demo.dto.AliyunVideoInfoDto;
import com.video.demo.entity.Result;
import com.video.demo.service.AliyunInfoService;
import com.video.demo.utils.ResultConstant;

/**
 * 
 * @author renwei
 *
 */
@RestController
@RequestMapping("/r/avic")
@Api(value = "/r/avic", description = "阿里云视频管理[renwei]")
public class AliyunVideoInfoController {
	Logger logger = LoggerFactory.getLogger(AliyunVideoInfoController.class);
	@Autowired
	private AliyunInfoService aliyunInfoService;
	/**
	 * 获取播放凭证(与获取播放地址播放一样)
	 * @author renwei
	 * @date 2017/11/20
	 * @return
	 */
	@ApiOperation(value = "获取播放凭证", notes = "获取播放凭证")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "videoId", value = "视频ID", paramType = "query", required = true, dataType = "String")})
	@RequestMapping(value="/getVideoPlayAuth",method=RequestMethod.GET)
	public Result getVideoPlayAuth(String videoId) {
		Result result =null;

	    GetVideoPlayAuthResponse response = null;
	    try {
	        response = aliyunInfoService.getVideoPlayAuth(videoId);
	        result = new Result(ResultConstant.SUCCESS, response);
	    } catch (Exception e) {
	    	logger.error("获取视频播放凭证失败"+e.getMessage());
	    	return new Result(ResultConstant.FAILED, e.getMessage());
	    }
	    return result;
	}
	
	/**
	 * 获取上传地址和上传凭证
	 * @author renwei
	 * @return
	 */
	@ApiOperation(value = "获取视频上传地址和上传凭证", notes = "获取视频上传地址和上传凭证")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileName", value = "视频名(必须包含后缀)",paramType = "query",required = true, dataType = "string"),
            @ApiImplicitParam(name = "title", value = "标题", paramType = "query",required = true, dataType = "string"),
            @ApiImplicitParam(name = "coverURL", value = "自定义视频封面URL地址",  paramType = "query",required = false, dataType = "string"),
            @ApiImplicitParam(name = "cateId", value = "分类ID",  paramType = "query",required = false, dataType = "int"),
            @ApiImplicitParam(name = "tags", value = "标签(多个标签以逗号隔开，不超过16个标签)", paramType = "query",required = false, dataType = "string"),
            @ApiImplicitParam(name = "description", value = "视频描述",  paramType = "query",required = false, dataType = "string"),
            @ApiImplicitParam(name = "fileSize", value = "视频大小",paramType = "query" ,required = false, dataType = "long")})
	@RequestMapping(value="/getVideoUploadAuth",method=RequestMethod.POST)
	public Result getVideoUploadAuth(String fileName,String title,String coverURL,Integer cateId,String tags,String description,Long fileSize) {
		AliyunVideoInfoDto aliyunVideoInfoDto = null;
		Result result =null;

		try {
			aliyunVideoInfoDto = aliyunInfoService.getVideoUploadInfo(fileName,title,coverURL,cateId,tags,description,fileSize);
			result = new Result(ResultConstant.SUCCESS, aliyunVideoInfoDto);
		} catch (Exception e) {
			logger.error("获取视频上传地址和上传凭证失败",e);
			return new Result(ResultConstant.FAILED, e.getMessage());
		}
        return result;
	}
	
	/**
	 * 刷新视频上传凭证
	 * 当网络异常导致文件上传失败时,可刷新上传凭证后再次执行上传操作
	 * @param videoId
	 * @author renwei
	 * @return
	 */
	@ApiOperation(value = "刷新视频上传凭证", notes = "当网络异常导致文件上传失败时,可刷新上传凭证后再次执行上传操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "videoId", value = "视频ID", paramType="query",required = true, dataType = "String")})
	@RequestMapping(value="/refreshVideoUploadAtuh",method=RequestMethod.POST)
	public Result refreshUploadAtuh(String videoId){
		String uploadAuth = null;
		Result result =null;

			try {
				uploadAuth = aliyunInfoService.refreshVideoUploadAtuh(videoId);
				result = new Result(ResultConstant.SUCCESS, uploadAuth);
			} catch (Exception e) {
				logger.error("刷新视频上传凭证失败",e);
				return new Result(ResultConstant.FAILED, e.getMessage());
			}
		
		return result;
	}
	
	/**
	 * 获取上传地址和上传凭证
	 * @author renwei
	 * @return
	 */
	@ApiOperation(value = "获取图片上传地址和上传凭证", notes = "获取图片上传地址和上传凭证")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "imageType", value = "图片类型,可选值cover:封面,watermark:水印",paramType = "query",required = true, dataType = "string"),
            @ApiImplicitParam(name = "imageExt", value = "图片文件扩展名,可选png,jpg,jpeg,默认png", paramType = "query",required = false, dataType = "string")})
	@RequestMapping(value="/getImageUploadAuth",method=RequestMethod.POST)
	public Result getImageUploadAuth(String imageType,String imageExt) {
		AliyunImageInfoDto aliyunImageInfoDto = null;
		Result result =null;

		try {
			aliyunImageInfoDto = aliyunInfoService.getImageUploadInfo(imageType,imageExt);
			result = new Result(ResultConstant.SUCCESS, aliyunImageInfoDto);
		} catch (Exception e) {
			logger.error("获取图片上传地址和上传凭证失败",e);
			return new Result(ResultConstant.FAILED, e.getMessage());
		}
        return result;
	}
	
	
	/**
	 * 转码成功回调函数
	 * @author renwei
	 * @return
	 * @throws IOException 
	 */
	@ApiOperation(value = "转码成功回调函数", notes = "转码成功回调函数")
	@RequestMapping(value="/transcodeComplete",method=RequestMethod.POST)
	public void transcodeComplete(HttpServletRequest request,@RequestBody JSONObject requestBody) throws IOException {
		JSONObject jobj = requestBody;
		String status = jobj.getString("Status");
		String videoId = jobj.getString("VideoId");
		String duration = jobj.getString("Duration");
		String fileUrl = jobj.getString("FileUrl");
		aliyunInfoService.transcodeComplete(status, videoId, duration, fileUrl);
	}
}
