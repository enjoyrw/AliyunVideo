package com.video.demo.enums;

/**
 * 阿里云错误信息枚举
 * @author renwei
 *
 */
public enum AliyunErrorEnums {
	ILLEGAL_STATUS("Forbidden.IllegalStatus", "视频状态无效"), 
	INVALID_FILE_NAME("InvalidParameter.FileName", "文件名FileName参数无效"),
	INIT_FAILED("Forbidden.InitFailed", "服务开通时账号初始化失败"),
	ADD_VIDEO_FAILED("AddVideoFailed", "创建视频信息失败，请稍后重试"),
	VIDEO_NOT_FOUND("InvalidVideo.NotFound", "视频不存在"),
	OPERATION_DENIED_SUSPENDED("OperationDenied.Suspended", "账号已欠费，请充值"),
	MISS_PARAMETER("MissingParameter", "缺少参数"),
	INVALID_PARAMETER("InvalidParameter", "参数无效"),
	TRANSCODE_FAIL("TranscodeFail", "转码失败，视频内容可能有误"),
	TRANSCODING("Transcoding", "视频转码中，请稍等"),
	OPERATION_FORBIDDEN("Forbidden", "用户无权限执行该操作"),
	OPERATION_DENIED("OperationDenied","账号未开通视频点播服务");
	String errorCode;
	String errorMessage;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	private AliyunErrorEnums(String errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	private AliyunErrorEnums() {
	}

	public static String getMsgByCode(String errorCode) {
		for (AliyunErrorEnums e : AliyunErrorEnums.values()) {
			if (e.getErrorCode().equals(errorCode)) {
				return e.getErrorMessage();
			}
		}
		return null;
	}

}
