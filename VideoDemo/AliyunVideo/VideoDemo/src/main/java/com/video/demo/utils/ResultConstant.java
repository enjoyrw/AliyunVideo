package com.video.demo.utils;



/**
 * 返回代码与错误信息
 * Created by CHQIU on 2017-06-20.
 */
public enum ResultConstant {


    //用户管理模块 10000
    EMPTY_LOGIN_NAME(10001, "登录名不能为空！"),
    EMPTY_PASSWORD(10002, "密码不能为空！"),
    INVALID_USERNAME(10003, "用户无效！"),
    INVALID_PASSWORD(10004, "密码错误！"),
    AUTHENTICATION_FAILED(10006, "登录认证失败，用户名或密码错误！"),

    INVALID_LOGIN(10300, "用户未登录！"),
    KICKOUT_LOGIN(10301, "用户在其他地方登录！"),
    SIMPLE_DENIED(10302, "被踢出！"),
    PERMISSION_DENIED(10303, "没有权限！"),
    INVALID_SESSION(12034, "会话已过期，请重新登录！"),


    //课业管理模块 20000
    //教学资源管理模块 30000
    //系统模版资源管理模块 40000
    //通用错误代码 <10000
    EMPTY_PARAM(104, "参数不能为空！"),
    FORMAT_ERROR(106, "格式错误！"),
    PARAM_ERROR(107, "参数有误！"),
    UNIQ_ERROR(108, "请勿添加重复项！"),
    GENERAL_ERROR(100, "其他错误！"),

    NO_DATA(404, "返回结果集中无数据！"),
    ERROR(500, "服务器正忙，请稍后再试试！"),
    INVALID_LENGTH(102, "长度无效！"),
    FAILED(0, "failed"),
    SUCCESS(1, "success");

    public int code;

    public String message;

    ResultConstant(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public ResultConstant setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return "ResultConstant{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
